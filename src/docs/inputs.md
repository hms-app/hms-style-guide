## Inputs


#### Inputs
<div class="form">
<div class="form-input">
  <input type="text" placeholder="Placeholder">
  <label></label>
</div><br>
<div class="form-input">
  <input type="number" value="123" class="success">
  <label></label>
</div><br>
<div class="form-input">
  <input type="email" value="wrong@email" class="error">
  <label></label>
</div><br>
<div class="form-input">
  <input type="text" value="Disabled" disabled>
  <label></label>
</div>


#### Textarea
<div class="form">
<div class="form-input">
  <textarea name="" id="" cols="50" rows="3"></textarea>
  <label for=""></label>
</div><br>
<div class="form-input">
  <textarea name="" id="" cols="50" rows="3" disabled></textarea>
  <label for=""></label>
</div>
</div>


#### Select
<div class="form-input">
  <select name="" id="">
    <option value="">01</option>
    <option value="">02</option>
    <option value="">03</option>
    <option value="">04</option>
    <option value="">05</option>
    <option value="">06</option>
  </select>
  <label for=""></label>
</div>

<div class="form-input">
  <select name="" id="" disabled>
    <option value="">01</option>
    <option value="">02</option>
    <option value="">03</option>
    <option value="">04</option>
    <option value="">05</option>
    <option value="">06</option>
  </select>
  <label for=""></label>
</div>
</div>

#### Checkbox
<form class="form" id="chkbx">
  <div class="form-input">
    <input type="checkbox" name="chkbox" value="chkbox" id="chkbox">
    <label for="chkbox">Lel</label>
  </div>
</form>


#### Radio buttons

<form class="form" id="radio">
  <div class="form-input">
    <input type="radio" name="kek" value="kek_value" id="kek">
    <label for="kek">Lel</label>
  </div>
  <div class="form-input">
    <input type="radio" name="kek" value="kek_value2" id="kek2">
    <label for="kek2">Lel2</label>
  </div>
</form>
