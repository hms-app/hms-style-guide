## Tables

#### Default
<table class="table">
  <thead>
    <tr><td>#</td><td>Col 1</td><td>Col 2</td><td>Col 3</td></tr>
  </thead>
  <tbody>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
  </tbody>
</table>

#### First column border
<table class="table f-column-border">
  <thead>
    <tr><td>#</td><td>Col 1</td><td>Col 2</td><td>Col 3</td></tr>
  </thead>
  <tbody>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
  </tbody>
</table>

#### Last column border
<table class="table l-column-border">
  <thead>
    <tr><td>#</td><td>Col 1</td><td>Col 2</td><td>Col 3</td></tr>
  </thead>
  <tbody>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
  </tbody>
</table>

#### Bordered
<table class="table bordered">
  <thead>
    <tr><td>#</td><td>Col 1</td><td>Col 2</td><td>Col 3</td></tr>
  </thead>
  <tbody>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
  </tbody>
</table>

#### Colored
<table class="table row-colored">
  <thead>
    <tr><td>#</td><td>Col 1</td><td>Col 2</td><td>Col 3</td></tr>
  </thead>
  <tbody>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
    <tr><td>01</td><td>ITEM 02</td><td>ITEM 03</td><td>ITEM 04</td></tr>
  </tbody>
</table>
