## Buttons

#### Default buttons
```markup
<button class="button smallest">Smallest</button>
<button class="button small">Small</button>
<button class="button">Default</button>
<button class="button big">Big</button>
<button class="button biggest">Biggest</button>
```
<div class ="container">
  <div class="row">
    <div class="col-12">
      <button class="button smallest">Smallest</button>
      <button class="button small">Small</button>
      <button class="button">Default</button>
      <button class="button big">Big</button>
      <button class="button biggest">Biggest</button>
    </div>
  </div>
</div>

#### Alert buttons + disabled
```markup
<button class="button error"></button>
<button class="button info"></button>
<button class="button warn"></button>
<button class="button success"></button>
```
<div class ="container">
  <div class="row">
    <div class="col-12">
      <button class="button error">Error</button>
      <button class="button info">Info</button>
      <button class="button warn">Warn</button>
      <button class="button success">Success</button>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <button class="button error disabled">Error</button>
      <button class="button info disabled">Info</button>
      <button class="button warn disabled">Warn</button>
      <button class="button success disabled">Success</button>
    </div>
  </div>
</div>

#### Bordered button
```markup
<button class="button bordered"></button>
<button class="button bordered error"></button>
<button class="button bordered info"></button>
<button class="button bordered warn"></button>
<button class="button success bordered "></button>
```
<div class ="container">
  <div class="row">
    <div class="col-12">
      <button class="button bordered">Bordered</button>
      <button class="button bordered error">Bordered</button>
      <button class="button bordered info">Bordered</button>
      <button class="button bordered warn">Bordered</button>
      <button class="button success bordered ">Bordered</button>
    </div>
  </div>
</div>

#### Fluid button
```markup
<button class="button fluid"></button>
```
<div class ="container">
  <div class="row">
    <div class="col-12">
      <button class="button fluid">Fluid</button>
    </div>
  </div>
</div>

#### Button group
```markup
<div class="button-group">
  <button class="button">Button 1</button>
  <button class="button">Button 2</button>
  ...
</div>
```
<div class="button-group">
  <button class="button">Button 1</button>
  <button class="button">Button 2</button>
  <button class="button">Button 2</button>
</div>
