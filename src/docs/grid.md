## Grid

#### Basic
```markup
  <div class="container">
    <div class="row">
      <div class="col-12">Col-12</div>
    </div>
    ...
  </div>
```
<div class="container grid-sample">
  <div class="row">
    <div class="col-1">Col-1</div>
  </div>
  <div class="row">
    <div class="col-2">Col-2</div>
  </div>
  <div class="row">
    <div class="col-3">Col-3</div>
  </div>
  <div class="row">
    <div class="col-4">Col-4</div>
  </div>
  <div class="row">
    <div class="col-5">Col-5</div>
  </div>
  <div class="row">
    <div class="col-6">Col-6</div>
  </div>
  <div class="row">
    <div class="col-7">Col-7</div>
  </div>
  <div class="row">
    <div class="col-8">Col-8</div>
  </div>
  <div class="row">
    <div class="col-9">Col-9</div>
  </div>
  <div class="row">
    <div class="col-10">Col-10</div>
  </div>
  <div class="row">
    <div class="col-11">Col-11</div>
  </div>
  <div class="row">
    <div class="col-12">Col-12</div>
  </div>
</div>

<div class="container grid-sample">
  <div class="row">
    <div class="col-6">Col-6</div>
    <div class="col-6">Col-6</div>
  </div>
  <div class="row ">
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
  </div>
</div>

<div class="container grid-sample">
  <div class="row">
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
    <div class="col-1">Col-1</div>
  </div>
  <div class="row">
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
    <div class="col-2">Col-2</div>
  </div>
  <div class="row">
    <div class="col-3">Col-3</div>
    <div class="col-3">Col-3</div>
    <div class="col-3">Col-3</div>
    <div class="col-3">Col-3</div>
  </div>
  <div class="row">
    <div class="col-4">Col-4</div>
    <div class="col-4">Col-4</div>
    <div class="col-4">Col-4</div>
  </div>
  <div class="row">
    <div class="col-5">Col-5</div>
    <div class="col-2">Col-2</div>
    <div class="col-5">Col-5</div>
  </div>
  <div class="row">
    <div class="col-6">Col-6</div>
    <div class="col-6">Col-6</div>
  </div>
</div>

<div class="container grid-sample">
  <div class="row">
    <div class="col-1">Col-1</div>
    <div class="col-11">Col-11</div>
  </div>
  <div class="row">
    <div class="col-2">Col-2</div>
    <div class="col-10">Col-10</div>
  </div>
  <div class="row">
    <div class="col-3">Col-3</div>
    <div class="col-9">Col-9</div>
  </div>
  <div class="row">
    <div class="col-4">Col-4</div>
    <div class="col-8">Col-8</div>
  </div>
  <div class="row">
    <div class="col-5">Col-5</div>
    <div class="col-7">Col-7</div>
  </div>
  <div class="row">
    <div class="col-6">Col-6</div>
    <div class="col-6">Col-6</div>
  </div>
  <div class="row">
    <div class="col-7">Col-7</div>
    <div class="col-5">Col-5</div>
  </div>
  <div class="row">
    <div class="col-8">Col-8</div>
    <div class="col-4">Col-4</div>
  </div>
  <div class="row">
    <div class="col-9">Col-9</div>
    <div class="col-3">Col-3</div>
  </div>
  <div class="row">
    <div class="col-10">Col-10</div>
    <div class="col-2">Col-2</div>
  </div>
  <div class="row">
    <div class="col-11">Col-11</div>
    <div class="col-1">Col-1</div>
  </div>
</div>

#### Center
```markup
  <div class="container">
    <div class="row center">
      <div class="col-5">Col-5</div>
      <div class="col-5">Col-5</div>
    </div>
  </div>
```
<div class="container grid-sample">
  <div class="row center">
    <div class="col-5">Col-5</div>
    <div class="col-5">Col-5</div>
  </div>
</div>

#### Space between
```markup
  <div class="container grid-sample">
    <div class="row space-between">
      <div class="col-5">Col-5</div>
      <div class="col-5">Col-5</div>
    </div>
  </div>
```
<div class="container grid-sample">
  <div class="row space-between">
    <div class="col-5">Col-5</div>
    <div class="col-5">Col-5</div>
  </div>
</div>

#### Space around
```markup
  <div class="container grid-sample">
    <div class="row space-around">
      <div class="col-5">Col-5</div>
      <div class="col-5">Col-5</div>
    </div>
  </div>
```
<div class="container grid-sample">
  <div class="row space-around">
    <div class="col-5">Col-5</div>
    <div class="col-5">Col-5</div>
  </div>
</div>

#### Vertical top
```markup
<div class="container grid-sample">
  <div class="row vert-top">
  ...
  </div>
</div>
```
<div class="container grid-sample">
  <div class="row vert-top">
    <div class="col-4">
      Lorem ipsum dolor
    </div>
    <div class="col-4">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit rem quaerat harum placeat ab asperiores veritatis in totam beatae,
    </div>
    <div class="col-4">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit rem quaerat harum placeat ab asperiores veritatis in totam beatae, explicabo dicta reprehenderit laborum sequi facilis debitis soluta ex delectus obcaecati.
    </div>
  </div>
</div>

#### Vertical center
```markup
<div class="container grid-sample">
  <div class="row vert-center">
  ...
  </div>
</div>
```
<div class="container grid-sample">
  <div class="row vert-center">
    <div class="col-4">
      Lorem ipsum dolor
    </div>
    <div class="col-4">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit rem quaerat harum placeat ab asperiores veritatis in totam beatae,
    </div>
    <div class="col-4">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit rem quaerat harum placeat ab asperiores veritatis in totam beatae, explicabo dicta reprehenderit laborum sequi facilis debitis soluta ex delectus obcaecati.
    </div>
  </div>
</div>

#### Vertical bottom
```markup
<div class="container grid-sample">
  <div class="row vert-bottom">
  ...
  </div>
</div>
```
<div class="container grid-sample">
  <div class="row vert-bottom">
    <div class="col-4">
      Lorem ipsum dolor
    </div>
    <div class="col-4">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit rem quaerat harum placeat ab asperiores veritatis in totam beatae,
    </div>
    <div class="col-4">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit rem quaerat harum placeat ab asperiores veritatis in totam beatae, explicabo dicta reprehenderit laborum sequi facilis debitis soluta ex delectus obcaecati.
    </div>
  </div>
</div>

#### Nested
```markup
<div class="container grid-sample">
  <div class="row">
    <div class="col-5">
      <div class="row">
        <div class="col-6">Col-6</div>
        <div class="col-6">Col-6</div>
      </div>
    </div>
    <div class="col-5">Col-5</div>
  </div>
</div>
```
<div class="container grid-sample">
  <div class="row">
    <div class="col-5">
      <div class="row">
        <div class="col-6">Col-6</div>
        <div class="col-6">Col-6</div>
      </div>
    </div>
    <div class="col-5">Col-5</div>
  </div>
</div>

#### Fixed width
```markup
  <div class="container">
    <div class="row">
      <div class="col-fixed f-300">Fixed 300px</div>
      <div class="auto">
        <div class="col-12">Col-12</div>
      </div>
    </div>
  </div>
```
<!-- Temporary hide -->
<!-- ```markup
  .f-300 {
    flex-basis: 300px;
  }
```
<div class="container grid-sample">
  <div class="row">
    <div class="col-fixed f-300">Fixed 300px</div>
    <div class="auto">
      <div class="col-12">Col-12</div>
    </div>
  </div>
</div>

<div class="container grid-sample">
  <div class="row">
    <div class="col-fixed f-300">Fixed 300px</div>
    <div class="row auto">
      <div class="col-2">Col-2</div>
      <div class="col-4">Col-4</div>
      <div class="col-6">Col-6</div>
    </div>
  </div>
</div>

<div class="container grid-sample">
  <div class="row">
    <div class="col-fixed f-700">Fixed 700px</div>
    <div class="row auto">
      <div class="col-2">Col-2</div>
      <div class="col-4">Col-4</div>
      <div class="col-6">Col-6</div>
    </div>
  </div>
</div> -->

#### Auto
```markup
  <div class="container">
    <div class="row">
      <div class="col-3">Col-2</div>
      <div class="auto">Auto</div>
    </div>
  </div>
```
<div class="container grid-sample">
  <div class="row">
    <div class="col-3">Col-3</div>
    <div class="auto">Auto</div>
  </div>
</div>

#### With gutter
```markup
<div class="container grid-sample">
  <div class="row with-padding">
    <div class="col-2">
      <div style="width: 100%; height: 100%;">
      ...
      </div>
    </div>
  </div>
</div>
```
<div class="container grid-sample">
  <div class="row with-padding">
    <div class="col-2"><div>Col-2</div></div>
    <div class="col-4"><div>Col-4</div></div>
    <div class="col-6"><div>Col-6</div></div>
  </div>
  <div class="row with-padding">
    <div class="col-3"><div>Col-3</div></div>
    <div class="col-3"><div>Col-3</div></div>
    <div class="col-3"><div>Col-3</div></div>
    <div class="col-3"><div>Col-3</div></div>
  </div>
</div>


