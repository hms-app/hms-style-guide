![](/img/logo.svg "Logo")
![](/img/new_text_logo.svg "TextLogo")
# Style guide

<section id="grid-sec">
<!-- partial:grid.md --><!-- partial -->
</section>&nbsp;

<section id="color-sec">
<!-- partial:colors.md --><!-- partial -->
</section>&nbsp;

<section id="typography-sec">
<!-- partial:typography.md --><!-- partial -->
</section>&nbsp;

<section id="buttons-sec">
<!-- partial:buttons.md --><!-- partial -->
</section>&nbsp;

<section id="inputs-sec">
<!-- partial:inputs.md --><!-- partial -->
</section>&nbsp;

<section id="tables-sec">
<!-- partial:tables.md --><!-- partial -->
</section>&nbsp;

<section id="alert-boxes-sec">
<!-- partial:alertbox.md --><!-- partial -->
</section>&nbsp;

<section id="blockquotes-sec">
<!-- partial:blockquotes.md --><!-- partial -->
</section>&nbsp;

<section id="rest-sec">
<!-- partial:rest.md --><!-- partial -->
</section>&nbsp;