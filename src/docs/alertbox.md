
## Alert boxes

```markup
<div class="alert-wrapper">
  <div class="alert-box">Default alert</div>
  <div class="alert-box error">Error alert</div>
  <div class="alert-box info">Info alert</div>
  <div class="alert-box warn">Warning alert</div>
  <div class="alert-box success">Success alert</div>
</div>
```
<div class="alert-wrapper">
  <div class="alert-box">Default alert</div>
  <div class="alert-box error">Error alert</div>
  <div class="alert-box info">Info alert</div>
  <div class="alert-box warn">Warning alert</div>
  <div class="alert-box success">Success alert</div>
</div>
