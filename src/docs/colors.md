## Colors

#### Brand colors

<div class="row">
  <div class="col-4">
    <div class="color brand">
      Brand color
      <div class="hex">#006171</div>
      <div class="rgb"></div>
    </div>
  </div>
  <div class="col-4">
    <div class="color brand-sec">
      Brand color secondary
      <div class="hex">#D71b36</div>
      <div class="rgb"></div>
    </div>
  </div>
  <div class="col-4">
    <div class="color brand-dark">
      Brand color dark
      <div class="hex">#001929</div>
      <div class="rgb"></div>
    </div>
  </div>
</div>

#### Font colors

<div class="row">
  <div class="col-4">
    <div class="color font">
      Font color main
      <div class="hex">#303638</div>
      <div class="rgb"></div>
    </div>
  </div>
  <div class="col-4">
    <div class="color font-sec">
      Font color secondary
      <div class="hex">#767676</div>
      <div class="rgb"></div>
    </div>
  </div>
  <div class="col-4">
    <div class="color font-light">
      Font color light
      <div class="hex">#FFF</div>
      <div class="rgb">rgb(255, 255, 255)</div>
    </div>
  </div>
</div>

#### Background colors

<div class="row">
  <div class="col-4">
    <div class="color bg">
      Background color main
      <div class="hex">#E1E3E6</div>
      <div class="rgb"></div>
    </div>
  </div>
  <div class="col-4">
    <div class="color bg-sec">
      Background color secondary
      <div class="hex">#F9F9F9</div>
      <div class="rgb"></div>
    </div>
  </div>
  <div class="col-4">
    <div class="color bg-dark">
      Background color dark
      <div class="hex">#46474A</div>
      <div class="rgb"></div>
    </div>
  </div>
</div>

#### Alerts

<div class="row">
  <div class="col-3">
    <div class="color err">
      Error
      <div class="hex">#D4343F</div>
      <div class="rgb"></div>
    </div>
  </div>
  <div class="col-3">
    <div class="color warn">
      Warning
      <div class="hex">#FFD24E</div>
      <div class="rgb"></div>
    </div>
  </div>
  <div class="col-3">
    <div class="color info">
      Info
      <div class="hex">#39C8DE</div>
      <div class="rgb"></div>
    </div>
  </div>
  <div class="col-3">
    <div class="color succ">
      Success
      <div class="hex">#65D776</div>
      <div class="rgb"></div>
    </div>
  </div>
</div>
