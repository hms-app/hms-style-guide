var gulp = require("gulp");
var sass = require("gulp-sass");
var browserSync = require("browser-sync").create();
var markdown = require("gulp-markdown");
var injectPartials = require("gulp-inject-partials");
var plumber = require('gulp-plumber');
var del = require('del');
var runSequence = require('run-sequence');
var sassImportJson = require('./utils/gulp-sass-import-json');


gulp.task("sass", sassTask);
gulp.task("cleanDist", cleanDistTask);
gulp.task("public", publicTask);
gulp.task("browserSyncInit", browserSyncInitTask);
gulp.task("cleanMarkdown", cleanMarkdownTask);
gulp.task("compileMarkdown", ["cleanMarkdown"], compileMarkdownTask);
gulp.task("inject", ["compileMarkdown"], injectTask);
gulp.task("reload", reloadTask);
gulp.task("watch", watchTask);

gulp.task("build",
  function(cb) {
    runSequence(["cleanDist", "public", "sass", "inject", "cleanMarkdown"], cb);
  });

gulp.task("default",
  function(cb) {
    runSequence(["cleanDist", "public", "sass", "inject", "cleanMarkdown", "browserSyncInit", "watch"], cb);
  });


function sassTask() {
  return gulp.src("./src/scss/main.scss")
             .pipe(sassImportJson({isScss: true}))
             .pipe(plumber())
             .pipe(sass({ includePaths: ["./src/scss/"] }))
             .pipe(plumber.stop())
             .pipe(gulp.dest("./dist/css"));
}

function browserSyncInitTask() {
  return browserSync.init(["./dist/css/*.css", "./dist/js/*.js", "./dist/index.html"], {
           server: { baseDir: "./dist" },
           port: 8888
         });
}

function cleanDistTask() {
  return del.sync("dist");
}

function publicTask() {
  return gulp.src("./public/**")
             .pipe(gulp.dest("./dist/"));
}

function reloadTask() {
  return browserSync.reload({stream: true});
}


function injectTask() {
  return gulp.src("./src/index.html")
             .pipe(injectPartials({removeTags: true}))
             .pipe(gulp.dest("./dist"));
}

function compileMarkdownTask() {
  return gulp.src("./src/docs/docs.md")
             .pipe(injectPartials({removeTags: true}))
             .pipe(markdown())
             .pipe(gulp.dest("./tmp/"));
}

function cleanMarkdownTask() {
  return del.sync("tmp");
}

function watchTask() {
  gulp.watch("./src/scss/**/*.*", ["sass", "reload"]);
  gulp.watch("./public/js/*.js", ["public", "reload"]);
  gulp.watch("./src/docs/**/*.md", ["inject", "reload"]);
}