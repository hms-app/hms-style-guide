(function() {
  'use strict';

  const mainMenu = document.getElementById("main-menu");
  const sections = Array.from(document.querySelectorAll("section")).reverse();

  toggleGridListener();
  toggleThemeListener();
  setUpMenu( mainMenu );
  window.scrollTo(window.scrollX, window.scrollY-1);
  window.addEventListener("scroll", function (e) {
    fixedHeaderListener();
    activeMenuItem(e, sections, mainMenu);
  }, false);



  function toggleGridListener() {
    document.getElementById("grid").addEventListener("click", function (e) {
      document.body.classList.toggle("grid");
      setStylesCookie();
    });
  }

  function toggleThemeListener() {
    document.getElementById("theme-toggle").addEventListener("change", function (e) {
      if(e.target.checked)
        document.body.classList.add("dark-theme");
      else
        document.body.classList.remove("dark-theme");
    });
  }

  function fixedHeaderListener() {
    var cl = document.body.classList;
    if( window.scrollY !== 0 )
      cl.add('scrolled');
    else
      cl.remove('scrolled');
  }

  function setUpMenu(mainMenu) {
    document.querySelectorAll("section").forEach( function(element, index, arr) {
      const header = element.getElementsByTagName("H2")[0];
      const link = document.createElement("a");
      const listElement = document.createElement("li");
      const subheaders = element.querySelectorAll("h4[id]");

      link.setAttribute("href", "#" + header.getAttribute("id"));
      link.setAttribute("id", header.getAttribute("id") + "-link");
      link.textContent = header.textContent;
      link.addEventListener("click", function(e) {
        e.preventDefault();
        var scrollY = window.scrollY;
        window.scrollTo( 0, getElementYPos(element));
      }, false);
      listElement.appendChild(link);

      if (subheaders.length > 0) {
        const submenu = document.createElement("ul");
        subheaders.forEach((subheader, index, arr) => {
          const listElement = document.createElement("li");
          const link = document.createElement("a");

          link.setAttribute("href", "#" + subheader.getAttribute("id"));
          link.setAttribute("id", header.getAttribute("id") + "-" + subheader.getAttribute("id") + "-link");
          link.textContent = subheader.textContent;
          link.addEventListener("click", function(e) {
            e.preventDefault();
            var scrollY = window.scrollY;
            window.scrollTo( 0, getElementYPos(subheader));
          }, false);
          listElement.appendChild(link);
          submenu.appendChild(listElement);
        });
        listElement.appendChild(submenu);
      }

      mainMenu.appendChild(listElement);
    });
  }

  function activeMenuItem(e, sections, mainMenu) {
    const windowScrollY = window.scrollY;
    const headers = sections.map( element => element.querySelector("H2") );
    const activeItem = headers.find( element => windowScrollY >= getElementYPos(element) );
    const id = activeItem ? activeItem.getAttribute("id") : "top";
    const activeLink = document.getElementById( id + "-link" );
    if ( !activeLink.classList.contains("active") ) {
      mainMenu.childNodes.forEach( el => el.firstChild.classList.remove("active") );
      activeLink.classList.add("active");
    }

    const subheaders = sections.map( element => Array.from( element.querySelectorAll("H4"))
                                                     .reverse())
                               .reduce((a, b) => a.concat(b));
    const activeSubItem = subheaders.find( element => windowScrollY >= getElementYPos(element) );
    const subId = activeSubItem ? activeSubItem.getAttribute("id") : "";
    const activeSubLink = document.getElementById( id + "-" + subId + "-link" );
    if ( activeSubLink ) {
      if ( !activeSubLink.classList.contains("active") ) {
        activeSubLink.parentNode.parentNode.childNodes.forEach( el => el.firstChild.classList.remove("active") );
        activeSubLink.classList.add("active");
      }
    }
  }

  function getElementYPos(element, scrollY) {
    if(scrollY === undefined) scrollY = 0;
    if(!element) return scrollY;
    return getElementYPos(element.offsetParent, scrollY + element.offsetTop);
  }

  // OLD FUNCTIONS

  function getCookie(cname) {
    return document.cookie.split(";").find( function(cookie) { return cookie.startsWith(" " + cname) } );
  }

  function setStylesCookie() {
    document.cookie = "styles=" + document.body.className;
  }

})();