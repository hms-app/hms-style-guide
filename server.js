var http = require('http');
var fs = require('fs');
var serveStatic = require('serve-static');
var express = require('express');

var app = express();

app.use(serveStatic('./dist', {index: 'index.html'}))

console.log("Style guide is running on port 8888");
app.listen(8888);